import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.WebSocketContainer;

import com.sun.security.ntlm.Client;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class Main extends Application implements EventHandler<ActionEvent> {

	Button buttonREST,buttonDOWN;
	Stage window;
	TextArea data;
	Label lblBestBIDS,lblBestASKS;
	
	static final String url = "https://api.bitso.com/v3/order_book/?book=btc_mxn";
	static final String urlWSS = "wss://ws.bitso.com";
	static final CountDownLatch messageLatch = new CountDownLatch(1);
	static final int bidsCount = 21, asksCount = 34;
	
	public static void main(String[] args) {
		launch(args);	
		
        try {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            System.out.println("Connecting to " + urlWSS);
            container.connectToServer(WebsocketClientEndpoint.class, URI.create(urlWSS));
            messageLatch.await(100, TimeUnit.SECONDS);
        } catch (DeploymentException | InterruptedException | IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
	
	
	@Override
	public void start(Stage primaryStage) throws Exception {

		window = primaryStage;
		window.setTitle("Crytocurrency Demo");
		
		
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10,10,10,10));
		grid.setVgap(8);
		grid.setHgap(8);	
		
		lblBestASKS = new Label();
		lblBestBIDS = new Label();

		
		buttonREST = new Button();
		buttonREST.setMaxWidth(150);
		buttonREST.setText("REST CALL");	
		
		GridPane.setConstraints(buttonREST, 0, 0);
		
		buttonDOWN = new Button();
		buttonDOWN.setMaxWidth(250);
		buttonDOWN.setText("UP & DOWN (NOT IMPLEMENTED)");	
		
		GridPane.setConstraints(buttonDOWN, 0, 2);
		
		data = new TextArea();
		data.setMaxWidth(300);
		data.setMaxHeight(300);
		data.setText("a�lksdjfa�lskdjfa,.\nasdfasdfasdfasdfasd");
		GridPane.setConstraints(data, 0, 1);
		
	
		// JAVA 8 - using lambda expressions
		buttonREST.setOnAction(e -> { 

			StringBuilder returnedData;
			try {
				returnedData = RestService.RestCallGET();
				if(returnedData != null) {				
					JSONObject json = new JSONObject(returnedData);
					data.setText(returnedData.toString());
					lblBestBIDS.setText("BEST BIDS: " + bidsCount);
					lblBestASKS.setText("BEST ASKS: " + asksCount);
					GridPane.setConstraints(lblBestASKS, 0, 5);
					GridPane.setConstraints(lblBestBIDS, 0, 4);
				}
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			
		});
		
		buttonDOWN.setOnAction(e -> { 
			System.out.println("aaaa");
			System.out.println("bbbbbb");
			
		});
		
		
		grid.getChildren().addAll(buttonREST,buttonDOWN,data,lblBestASKS,lblBestBIDS);		
		Scene scene = new Scene(grid,350,300);
		
		window.setScene(scene);
		window.show();
		
		
	}

	@Override
	public void handle(ActionEvent event) {

	}
	

}
	

